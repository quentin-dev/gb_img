FROM ubuntu:disco

RUN apt update
RUN apt install software-properties-common  git build-essential sdcc sdcc-libraries gcc cmake -y

RUN git clone --recursive https://github.com/Snaipe/Criterion

WORKDIR Criterion/

RUN mkdir build/
WORKDIR build/

RUN cmake -DCMAKE_INSTALL_PREFIX=/usr ..
RUN cmake --build .

RUN make install

WORKDIR ../